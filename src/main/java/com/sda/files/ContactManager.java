package com.sda.files;

import com.sda.contacts.Contact;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class ContactManager {
    private static final String CONTACTS = "saves/contacts.bin";
    private static final String CONTACTS_IMAGES = "saves/images/";
    private Logger logger = LogManager.getLogger(ContactManager.class);

    public Map<String, Contact> loadContacts() {
        Map<String, Contact> result = new TreeMap<>();
        try {
            FileInputStream fis = new FileInputStream(CONTACTS);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Contact contact = null;
            while ((contact = (Contact) ois.readObject()) != null) {
                result.put(contact.getFullName(), contact);
            }
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            logger.error("Could not load contacts.");
            e.printStackTrace();
        }
        return result;
    }

    public void updateContacts(Map<String, Contact> contacts) {
        try {
            FileOutputStream fos = new FileOutputStream(CONTACTS);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for (Contact contact : contacts.values()) {
                oos.writeObject(contact);
            }
            oos.close();
        } catch (IOException e) {
            logger.error("Could not update contacts.");
            e.printStackTrace();
        }
    }

    public boolean saveContactImage(Image image, String uuid){
        try {
            BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
            ImageIO.write(bImage, "jpg", new File(CONTACTS_IMAGES+uuid+".jpg"));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Image loadContactImage(String uuid){
        Image image = null;
        try {
            BufferedImage bImage = ImageIO.read(new File(CONTACTS_IMAGES+uuid+".jpg"));
            image = SwingFXUtils.toFXImage(bImage, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }
}
