package com.sda.weather;

public class WeatherDTO {
    private Integer minTemp;
    private Integer maxTemp;
    private Integer theTemp;
    private WeatherState weatherState;

    public Integer getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Integer minTemp) {
        this.minTemp = minTemp;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Integer getTheTemp() {
        return theTemp;
    }

    public void setTheTemp(Integer theTemp) {
        this.theTemp = theTemp;
    }

    public WeatherState getWeatherState() {
        return weatherState;
    }

    public void setWeatherState(WeatherState weatherState) {
        this.weatherState = weatherState;
    }

    @Override
    public String toString() {
        return "WeatherDTO{" +
                "minTemp=" + minTemp +
                ", maxTemp=" + maxTemp +
                ", theTemp=" + theTemp +
                ", weatherState=" + weatherState +
                '}';
    }
}
