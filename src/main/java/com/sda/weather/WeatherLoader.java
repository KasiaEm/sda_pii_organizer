package com.sda.weather;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class WeatherLoader {
    private Logger logger = LogManager.getLogger(WeatherLoader.class);

    public WeatherDTO loadWeather(String localization) {
        String localisationData = loadJSONString("https://www.metaweather.com/api/location/search/?query=" + localization);
        if(!localisationData.isEmpty()) {
            JSONArray localisationJSONArray = new JSONArray(localisationData);
            JSONObject localisationJSON = localisationJSONArray.getJSONObject(0);
            String woeid = localisationJSON.get("woeid").toString();
            WeatherDTO weatherDTO = new WeatherDTO();
            if (woeid != null) {
                JSONObject weatherJSON = new JSONObject(loadJSONString("https://www.metaweather.com/api/location/" + woeid));
                JSONArray consolidatedWeather = weatherJSON.getJSONArray("consolidated_weather");
                JSONObject weatherTodayJSON = consolidatedWeather.getJSONObject(0);
                logger.info("[Weather today]" + weatherTodayJSON);
                weatherDTO.setMaxTemp(weatherTodayJSON.getInt("max_temp"));
                weatherDTO.setMinTemp(weatherTodayJSON.getInt("min_temp"));
                weatherDTO.setTheTemp(weatherTodayJSON.getInt("the_temp"));
                String weatherState = weatherTodayJSON.getString("weather_state_abbr");
                switch (weatherState) {
                    case "sn":
                        weatherDTO.setWeatherState(WeatherState.SNOW);
                        break;
                    case "sl":
                    case "h":
                    case "hr":
                    case "lr":
                    case "s":
                        weatherDTO.setWeatherState(WeatherState.RAIN);
                        break;
                    case "t":
                        weatherDTO.setWeatherState(WeatherState.STORM);
                        break;
                    case "hc":
                    case "lc":
                        weatherDTO.setWeatherState(WeatherState.CLOUDS);
                        break;
                    default:
                        weatherDTO.setWeatherState(WeatherState.SUN);
                }
            }
            logger.info("[WeatherDTO] " + weatherDTO);
            return weatherDTO;
        }
        logger.error("Connection problem.");
        return null;
    }

    private String loadJSONString(String url) {
        StringBuilder jsonText = new StringBuilder();
        try (InputStream is = new URL(url).openStream()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            br.lines().forEach(jsonText::append);
        } catch (IOException | JSONException e) {
            logger.error("Could not load weather.");
            e.printStackTrace();
        }
        logger.info(jsonText);
        return jsonText.toString();

    }
}
